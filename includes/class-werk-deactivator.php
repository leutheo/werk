<?php

/**
 * Fired during plugin deactivation
 *
 * @link       kraft
 * @since      1.0.0
 *
 * @package    Werk
 * @subpackage Werk/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Werk
 * @subpackage Werk/includes
 * @author     theo <garneaubienvenu@gmail.com>
 */
class Werk_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
