<?php

/**
 * Fired during plugin activation
 *
 * @link       kraft
 * @since      1.0.0
 *
 * @package    Werk
 * @subpackage Werk/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Werk
 * @subpackage Werk/includes
 * @author     theo <garneaubienvenu@gmail.com>
 */
class Werk_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
