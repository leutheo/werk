<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       kraft
 * @since      1.0.0
 *
 * @package    Werk
 * @subpackage Werk/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
